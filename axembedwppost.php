<?php
if (!defined('_PS_VERSION_'))
	exit ;

Class AxEmbedWpPost extends Module {

	public function __construct() {
		$this -> name = 'axembedwppost';
		$this -> tab = 'front_office_features';
		$this -> version = '1.0';
		$this -> author = 'Axome';
		$this -> bootstrap = true;
		$this -> displayName = $this -> l("Intégration d'un article Wordpress");
		$this -> description = $this -> l('Ajoute un article tiré de wordpress dans la description des produits.');
		$this -> confirmUninstall = $this -> l('Souhaitez vous vraiment désinstaller ce module ?');

		parent::__construct();
	}

	public function install() {
		parent::install();
		$this -> registerHook('displayAdminProductsExtra');
		$this -> registerHook('displayProductTabContent');
		$this -> createTable();
		return true;
	}

	public function createTable() {//on install
		$sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'embed_post_wp (
			id_product int(11) NOT NULL,
			id_post int(11) NOT NULL,
			PRIMARY KEY (id_product))
			AUTO_INCREMENT=1 ;';
		Db::getInstance() -> execute($sql);
	}

	///////////////////////////
	/*			Hook		 */
	//////////////////////////

	public function hookDisplayProductTabContent($params) {//product field
		$this -> assignProductTabContent();
		return $this -> display(__FILE__, 'displayProductTabContent.tpl');
		//va automatiquement chercher la template displayProductTabContent.tpl dans son bon dossier
	}

	public function hookDisplayAdminProductsExtra() {//admin field
		$this -> checkProductRegistered();
		$this -> assignConfiguration();
		$this -> processConfiguration();
		return $this -> display(__FILE__, 'views/templates/admin/displayAdminProductsExtra.tpl');
	}

	public function getContent() {//Configuration module
		$this -> adminConfiguration();
		$this -> adminAssign();
		return $this -> display(__FILE__, 'configureModule.tpl');
	}

	//////////////////////////////
	/*		admin side			*/
	//////////////////////////////

	public function adminConfiguration() {
		if (Tools::isSubmit('enter_url_wp')) {//nom du bouton submit du formulaire correspondant
			$url = Tools::getValue('wp_url');
			Configuration::updateValue('ax_embed_wp_post_url', $url);
		}
		$this -> context -> smarty -> assign('confirmation', 'ok');
	}

	public function adminAssign() {
		$url = Configuration::get('ax_embed_wp_post_url');
		$this -> context -> smarty -> assign('ax_wp_url', $url);
	}

	public function checkProductRegistered() {
		$register = true;
		$id_product = Tools::getValue('id_product');
		if ($id_product == 0) {
			$register = false;
		}
		$this -> context -> smarty -> assign('productRegister', $register);
	}

	//////////////////////////////
	/*		product side		*/
	//////////////////////////////

	public function assignConfiguration() {
		$id_product = Tools::getValue('id_product');
		$sql = 'SELECT id_post FROM ' . _DB_PREFIX_ . 'embed_post_wp WHERE id_product = ' . (int)$id_product;
		$id_post = Db::getInstance() -> executeS($sql);

		if ($id_post) {
			$this -> context -> smarty -> assign('id_post', $id_post[0]['id_post']);
		}
	}

	public function processConfiguration() {
		if (Tools::isSubmit('axembedwppost_form_admin'))//nom du bouton submit du formulaire correspondant
		{
			$id_product = Tools::getValue('id_product');
			$post_wp = Tools::getValue('url_wp_post');
			$sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'embed_post_wp WHERE id_product = ' . (int)$id_product;
			$query = array('id_product' => $id_product, 'id_post' => $post_wp);
			$product = Db::getInstance() -> executeS($sql);

			if (empty($product)) {
				Db::getInstance() -> insert('embed_post_wp', $query);
			} elseif ($product) {
				$where = 'id_product = ' . $id_product;
				Db::getInstance() -> update('embed_post_wp', $query, $where);
			}
		}
		$this -> context -> smarty -> assign('confirmation', 'ok');
	}

	public function assignProductTabContent() {
		$id_product = Tools::getValue('id_product');
		$sql = 'SELECT id_post FROM ' . _DB_PREFIX_ . 'embed_post_wp WHERE id_product = ' . (int)$id_product;
		$error = false;
		$array_post_wp = Db::getInstance() -> executeS($sql);
		if ($array_post_wp) {
			$id_post_wp = $array_post_wp[0]['id_post'];
			$url = Configuration::get('ax_embed_wp_post_url');
			$data = $this -> getDataPostWP($id_post_wp, $url);

			if ($data) {
				$this -> context -> smarty -> assign('title', $data['title']);
				$this -> context -> smarty -> assign('content', $data['content']);
				$this -> context -> smarty -> assign('date', $data['date_heure'][0]);
				$this -> context -> smarty -> assign('heure', $data['date_heure'][1]);
				$this -> context -> smarty -> assign('img_link', $data['img_link']);
			} else {
				$error = true;
			}
		} else {
			$error = true;
		}
		$this -> context -> smarty -> assign('error', $error);
		//Si erreur = true alors on affiche un msg d'erreur dans le tpl
	}

	public function getDataPostWP($id_post, $url) { //recupere les données du post à récuperer (passé en parametre) en JSON et les renvois sous forme de tableau
		$tableau = array();
		$url_post = $url . 'wp-json/wp/v2/posts/' . $id_post;

		if (file_get_contents($url_post) == true) {
			$post_json = file_get_contents($url_post);
			$data = json_decode($post_json, true);
			$img_source = $data['content']['rendered'];
			
			/*******************************************/
							//REGEX//

			preg_match('/<img[^>]+>/i', $img_source, $lien);
			//take content <img .... />
			preg_match('/src="([^"]*)"/', $lien[0], $img_url);
			//take content src="..."
			$img_link = $img_url[1];

			/*******************************************/

			$title = $data['title']['rendered'];
			$excerpt = $data['excerpt']['rendered'];
			$date = $data['date'];
			$date_heure = explode('T', $date);
			$tableau = array('title' => $title, 'content' => $excerpt, 'date_heure' => $date_heure, 'img_link' => $img_link);
		}
		return $tableau;
	}

}
