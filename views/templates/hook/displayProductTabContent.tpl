<section class="page-product-box">
	<h3 class="page-product-heading"> La recette </h3>
	<div class="rte">
		{if $error == false}
			{if $img_link}<div><img src={$img_link} height="200px" /></div>{/if}
			<h4>{$title}</h4>
			<span>{$date}</span>
			<p>{$content}</p>
		{else}
			<div class="alert alert-warning">
				Erreur ! L'article correspondant n'a pas été trouvé où n'existe plus.
			</div>
		{/if}
	</div>
</section>