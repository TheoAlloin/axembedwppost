<fieldset>
	<h2>Configurez l'url du site distant à connecter</h2>
	<div class="panel">
		<div class="form-group">
			<form action="" method="POST">
				<label class="control-label col-lg-6 alert alert-info">
				Attention ! Seul les sites wordpress sont pris en compte. <br />Veuillez vérifier que l'API V2 REST soit installé sur le site fournisseur.
				</label>
				<input type="text" id="wp_url" name="wp_url" value="{$ax_wp_url}" placeholder="URL du site wordpress à configurer" />
				<br />
				<center><input class="btn btn-default" type="submit" name="enter_url_wp" value="Save"></center>
			</form>
		</div>
	</div>
	
	{if isset($confirmation)}
		<div class="alert alert-success">Informations mises à jour</div>
	{/if}
</fieldset>