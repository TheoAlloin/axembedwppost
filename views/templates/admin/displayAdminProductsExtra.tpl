<fieldset>
{if $productRegister == true }
	<div class="panel">
		<div class="panel-heading">
			<legend><img src="../img/admin/cog.gif" alt="" width="16" /> Intégrez un article wordpress</legend>
		</div>
		<form action="" method="post">
			<div class="form-group clearfix">
				<label class="col-lg-3">Identifiant du post wordpress à intégrer : </label>
				<div class="col-lg-9">
					<input type="text" id="url_wp_post" name="url_wp_post" value="{$id_post}"/>
				</div>
			</div>
			<div class="panel-footer">
				<input class="btn btn-default pull-right" type="submit" name="axembedwppost_form_admin" value="Sauvegarder" />
			</div>
		</form>
	</div>
{elseif $productRegister == false}
	<div class="alert alert-warning">
		<button type="button" class="close" data-dismiss="alert">×</button>
		Il y a 1 avertissement.
		<ul style="display:block;" id="seeMore">
			<li>Vous devez enregistrer ce produit avant de le personnaliser.</li>
		</ul>
	</div>
{/if}	
{if isset($confirmation)}
	<div class="alert alert-success">Informations mises à jour</div>
{/if}

</fieldset>
